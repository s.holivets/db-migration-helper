# db-re-encrypt-helper


Tool for helping to encoding some personal info in database

# Usage
`node . -t <table name> -c <column name> [-a <algorithm>]`

Encryption Algorithm:
  - sha512 - hash
  - bcrypt - hash
  - uuid - uuid based on random string
  - no-crypt - random string in plain text
  - '' - clean up information
