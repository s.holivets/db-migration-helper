#!/usr/bin/env node

require('dotenv').config();

const bcrypt = require('bcrypt');
const yargs = require('yargs');
const { knex } = require('./database');
const { sha512 } = require('js-sha512');
const crypto = require('crypto');
const getUuid = require('uuid-by-string');

const SALT_ROUNDS = 10;
const pepper = process.env.PEPPER;
const exit = (code = 0) => process.exit(code);
const encode = async (str) => bcrypt.hash(`${ str }${ pepper }`, SALT_ROUNDS);
const generateRandomString = (length = 32) => crypto.randomBytes(length).toString('hex');
const secureHash = (str) => sha512(`${ process.env.SALT }${ str }`);

const options = yargs
  .usage("Usage: -n <name>")
  .option("t", {
    alias: "table",
    describe: "Table Name",
    type: "string",
    demandOption: true,
  })
  .option("c", {
    alias: "column",
    describe: "Column Name",
    type: "string",
    demandOption: true
  })
  .option("a", {
    alias: "algorithm",
    describe: `
      Encryption Algorithm:
      - sha512 - hash
      - bcrypt - hash
      - uuid - uuid based on random string
      - no-crypt - random string in plain text
      - '' - clean up information
    `,
    type: "string",
  })
  .argv;

async function getData(table, column) {
  try {
    const data = await knex(table).select()
      .whereNotNull(column);

    if (!data) {
      exit();
    }

    return data;
  } catch (e) {
    console.error(e);
    exit(1);
  }

}

/**
 * node . -t <table> -c <column> -a <algorithm>
 * Algorithm: sha512, bcypt, uuid, ''
 * @returns {Promise<null>}
 */
async function main() {
  const { table, column, algorithm } = options;
  console.log(`Algorithm: ${ algorithm || 'Clear data' }`);
  if (!algorithm) {
    await knex(table)
      .whereNotNull(column)
      .update({ [column]: '', });
  } else {
    const data = await getData(table, column);

    for (const row of data) {
      console.log(row.id);
      const randomString = generateRandomString();
      let resultString = '';

      switch (algorithm) {
        case 'sha512': {
          resultString = await secureHash(randomString);

          break;
        }
        case 'bcrypt': {
          resultString = await encode(randomString);

          break;
        }
        case 'uuid': {
          resultString = getUuid(randomString);

          break;
        }
        case 'no-crypt': {
          resultString = randomString;

          break;
        }
        default: {
          break;
        }
      }

      await knex(table)
        .where({ id: row.id })
        .update({ [column]: resultString, });
    }
  }

  console.log('DONE');

  exit();
}

main();
