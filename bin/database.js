const knex = require('knex');

console.log(process.env.DATABASE_URL);

const knexInstance = knex({
  client: "pg",
  connection: process.env.DATABASE_URL,
});

module.exports = {
  knex: knexInstance
};
